resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  instance_tenancy = var.tenancy
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route" "route" {
  route_table_id         = aws_vpc.vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.internet_gateway.id
}

# resource "aws_nat_gateway" "_" {
#   subnet_id = aws_subnet._.id
# }


# resource "aws_route_table" "_" {
#   vpc_id = aws_vpc._.id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway._.id
#     # nat_gateway_id = aws_nat_gateway._.id
#   }
# }

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.public_cidr
  availability_zone = var.public_az
}

resource "aws_subnet" "private_subnet_1" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private_cidr_1
  availability_zone = var.private_az_1
}

resource "aws_subnet" "private_subnet_2" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.private_cidr_2
  availability_zone = var.private_az_2
}


resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.vpc.id
}


resource "aws_route_table_association" "public_route_table_association" {
  subnet_id      =  aws_subnet.public.id
  route_table_id = aws_vpc.vpc.main_route_table_id
}

resource "aws_route_table_association" "private_route_table_association_1" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_route_table.id
}

resource "aws_route_table_association" "private_route_table_association_2" {
  subnet_id = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_route_table.id
}


#https://medium.com/@mda590/aws-routing-101-67879d23014d