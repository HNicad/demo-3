variable "vpc_cidr" {
  type        = string
  description = "CIDR block for vpc"
}

variable "tenancy" {
  type = string
  default = "default"   
}

variable "enable_dns_support" {
  type    = bool
  default = true
}

variable "enable_dns_hostnames" {
  type = bool
  default = true
}


variable "public_cidr" {

}

variable "public_az" {
  
}

variable "private_cidr_1" {
  
}

variable "private_cidr_2" {
  
}

variable "private_az_1" {
  
}

variable "private_az_2" {
  
}
