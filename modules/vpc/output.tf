output "vpc_id" {
    value = aws_vpc.vpc.id
}

output "vpc_cidr" {
    value = aws_vpc.vpc.cidr_block
}


output "public_subnet_id" {
    value = aws_subnet.public.id
}

output "private_subnet_1" {
    value = aws_subnet.private_subnet_1.id
}

output "private_subnet_2" {
    value = aws_subnet.private_subnet_2.id  
}

output "igw_id" {
    value = aws_internet_gateway.internet_gateway.id
}