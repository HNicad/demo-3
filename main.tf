provider "aws" {
    region = "us-east-2"
}


module "vpc" {
    source = "./modules/vpc/"
    vpc_cidr = "192.168.0.0/16"
    public_cidr = "192.168.1.0/24"
    private_cidr_1 = "192.168.2.0/24"
    private_cidr_2 = "192.168.3.0/24"
    

    public_az = data.aws_availability_zones.available.names[0]
    private_az_1 = data.aws_availability_zones.available.names[1]
    private_az_2 = data.aws_availability_zones.available.names[0]
}

data "aws_availability_zones" "available" {
  state = "available"
}