variable "ec2_count" {
  default = "1"
}

variable "ami_id" {
    default = "ami-077e31c4939f6a2f3" 
}

variable "instance_type" {
  default = "t2.micro"
}