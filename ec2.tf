resource "aws_instance" "ec2" {
    ami = var.ami_id
    instance_type = var.instance_type
    subnet_id = module.vpc.public_subnet_id
    associate_public_ip_address = true
    key_name = "develop"
    vpc_security_group_ids = [aws_security_group.ec2-sg.id]
    depends_on = [
      module.vpc.vpc_id, module.vpc.igw_id, aws_db_instance.default
    ]
    user_data = <<EOF
        
        #!/bin/sh
        sudo yum update -y.
        sudo yum install docker -y
        sudo service docker start
        sudo docker pull hnijad/app:latest
        sudo docker run -d -p 80:8080 -e MYSQL_USER=petclinic -e MYSQL_PASS=petclinic -e MYSQL_URL=jdbc:mysql://${aws_db_instance.default.address}:3306/petclinic hnijad/app
    EOF
}
    
resource "aws_security_group" "ec2-sg" {
    name = "security-group"
    vpc_id = module.vpc.vpc_id

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        description = "SSH"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        description = "HTTP"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        protocol    = "-1"
        from_port   = 0
        to_port     = 0
        cidr_blocks = ["0.0.0.0/0"]
     }
}